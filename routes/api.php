<?php

use App\Http\Controllers\ClasssController;
use App\Http\Controllers\RegisterationController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\AttendanceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

    Route::post('test',[StudentController::class,'storeMany']);


Route::post('login',[RegisterationController::class,'login'])->name('login');
Route::post('register',[RegisterationController::class,'register'])->name('register');
Route::post('logout',[RegisterationController::class,'logout'])->name('logout');
// Route::group(['middleware' => 'auth:sanctum'],function () {

    Route::get('students',[StudentController::class,'index'])->name('students.index');
    Route::post('students/store',[StudentController::class,'store'])->name('students.store');
    Route::post('students/storeMany',[StudentController::class,'storeMany'])->name('students.storeMany');
    Route::get('students/edit/{id}',[StudentController::class,'edit'])->name('students.edit');
    Route::post('students/update',[StudentController::class,'update'])->name('students.update');
    Route::post('students/destroy',[StudentController::class,'destroy'])->name('students.destroy');

    Route::get('students/attendance',[StudentController::class,'index_attendance'])->name('students.index_attendance');

    Route::get('class',[ClasssController::class,'index'])->name('class.index');
    Route::post('class/store',[ClasssController::class,'store'])->name('class.store');
    Route::get('class/edit/{id}',[ClasssController::class,'edit'])->name('class.edit');
    Route::post('class/update',[ClasssController::class,'update'])->name('class.update');
    Route::post('class/destroy',[ClasssController::class,'destroy'])->name('class.destroy');




    Route::post('attendance/store',[AttendanceController::class,'store'])->name('attendance.store');
    Route::post('attendance/storeMany',[AttendanceController::class,'storeMany'])->name('attendance.storeMany');







// });
