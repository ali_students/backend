<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function classs()
    {
        return $this->belongsTo(Classs::class);
    }

    public function attendance()
    {
        return $this->hasMany(Attendance::class);
    }
}
