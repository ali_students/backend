<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::orderBy('created_at','asc')->with("classs")->get();
        return response()->json(['students' => $students],200);
    }

    public function index_attendance()
    {
        $students = Student::orderBy('created_at','asc')->with("attendance")->get();
        return response()->json(['students' => $students],200);
    }

    public function store(Request $request)
    {
       $student = Student::create($request->all());
       return response()->json(['student' => $student],200);
    }


    public function storeMany(Request $request)
    {
    $students = $request->input('data');

    $students_saved=[];

    foreach ($students as $studentData) {
        $student = new Student();

        $student->name = $studentData['name'];
        $student->notes = $studentData['notes'];
        $student->father = $studentData['father'];
        $student->address = $studentData['address'];
        $student->phone = $studentData['phone'];
        $student->birth_date = $studentData['birth_date'];
        $student->classs_id = $studentData['classs_id'];

        $student->save();
        // $students_saved->array_push($student);
    }

    return response()->json([
        'students' => $students_saved
    ]);

    }


    public function update(Request $request)
    {
        $student = Student::find($request->id)->update($request->all());
        return response()->json(['student' => $student],200);
    }

    public function destroy(Request $request)
    {
        Student::find($request->id)->delete();
        return response()->json([],200);
    }

}

