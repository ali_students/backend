<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attendance;


class AttendanceController extends Controller
{


    public function store(Request $request)
    {
       $attendance = Attendance::create($request->all());
       return response()->json(['attendance'=>$attendance],200);
    }


    public function storeMany(Request $request)
    {
    $attendancees = $request->input('data');

    foreach ($attendancees as $attendance) {
    try {
        $newAttendance = new Attendance();

        $newAttendance->student_id = $attendance['student_id'];
        $newAttendance->date = $attendance['date'];
        $newAttendance->save();
    } catch ( \Exception $e) {
         continue;
    }
    }
    return response()->json(['message'=>"saved "],200);

}

}
