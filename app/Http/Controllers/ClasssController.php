<?php

namespace App\Http\Controllers;

use App\Models\Classs;
use Illuminate\Http\Request;

class ClasssController extends Controller
{
    public function index()
    {

        $classes = Classs::orderBy('created_at','DESC')->withCount('students')-> get();
        return response()->json([ $classes],200);
    }



    public function store(Request $request)
    {
       $class = Classs::create($request->all());
       return response()->json(['class'=>$class],200);
    }

    public function edit()
    {
        //
    }

    public function update(Request $request)
    {
        $class = Classs::find($request->id)->update($request->all());
        return response()->json(['class' => $class],200);
    }

    public function destroy(Request $request)
    {
        Classs::find($request->id)->delete();
        return response()->json([],200);
    }
}
